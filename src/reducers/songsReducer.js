export const songsReducer = (state = [], action) => {
    switch (action.type) {
        case 'GET_SONGS':
            return [...state]
        case 'ADD_SONG':
            return [...state, action.payload]
        default:
            return state
    }
}