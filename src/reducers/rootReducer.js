import {combineReducers} from 'redux'
import {songsReducer} from "./songsReducer";
import {selectedSongReducer} from "./selectedSongReducer";

export const rootReducer = combineReducers({
    songs: songsReducer,
    selectedSong: selectedSongReducer
})