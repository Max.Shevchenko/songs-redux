export const selectedSongReducer = (state = [], action) => {
    switch (action.type) {

        case 'SELECT_SONG':
            state = action.payload
            return state

        default:
            return state
    }
}