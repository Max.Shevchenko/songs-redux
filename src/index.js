import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import App from './components/App/App'
import {createStore} from 'redux'
import {rootReducer} from "./reducers/rootReducer";
import {composeWithDevTools} from 'redux-devtools-extension'

import '../node_modules/bootstrap/scss/bootstrap.scss'

const store = createStore(rootReducer, composeWithDevTools())

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,

    document.getElementById('root')
)