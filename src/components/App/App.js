import React from 'react';
import {Container, Row, Col} from 'react-bootstrap'
import SongsForm from "../SongsForm";
import SongsList from "../SongsList";
import SongsListContainer from '../../containers/SongsListContainer'
import SelectedSong from '../SelectedSong'

const App = () => {
    return (
        <Container>
            <Row>
                <Col>
                    <SongsForm/>

                </Col>
            </Row>
            <Row>
                <Col lg={6} md={12}>
                    <SongsListContainer/>
                </Col>
                <Col lg={6} md={12}>
                    <SelectedSong/>
                </Col>
            </Row>
        </Container>
    );
};

export default App;

