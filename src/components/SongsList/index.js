import React from 'react';
import {Table, Button} from 'react-bootstrap'
import Song from '../Song/Song'


const renderSongs = songs => {
    return songs.length ?

        songs.map(song => (

            <Song key={song.name} name={song.name} duration={song.duration}/>
        ))
        :

        <tr>
            <td colSpan={3} className="text-center">
                No songs
            </td>
        </tr>
}


const SongsList = (props) => {

    return (
        <Table className="mt-5">
            <thead>
            <tr>
                <th className="text-center">
                    Name
                </th>
                <th className="text-center">
                    Duration
                </th>
                <th className="text-center">
                    Select
                </th>
            </tr>
            </thead>
            <tbody>
            {renderSongs(props.songs)}
            </tbody>
        </Table>
    );
};


export default SongsList;
