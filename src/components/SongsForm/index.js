import React, {Component} from 'react'
import {addSong} from "../../actions/songsActions";
import {connect} from 'react-redux'
import {Col, Row, Form, FormGroup, FormControl, Button} from 'react-bootstrap'

class SongsForm extends Component {

    state = {
        name: '',
        duration: ''
    }


    handleNameChange = (e) => {
        this.setState({
            name: e.target.value
        })
    }


    handleDurationChange = (e) => {
        this.setState({
            duration: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.props.addSong(this.state)
    }


    render() {


        return (
            <Form onSubmit={this.handleSubmit}>
                <FormGroup as={Row}>
                    <Col md={6}>
                        <FormControl onChange={this.handleNameChange}
                                     type="text"
                                     value={this.state.name}
                                     placeholder="Enter song name"
                                     className="mt-3"
                        />
                    </Col>

                    <Col md={6}>
                        <FormControl onChange={this.handleDurationChange}
                                     type="text"
                                     value={this.state.duration}
                                     placeholder="Enter duration"
                                     className="mt-2 mt-md-3"
                        />
                    </Col>
                </FormGroup>
                <div className="w-100 text-center">
                    <Button varaint="primary" type="submit">
                        Add song
                    </Button>
                </div>
            </Form>
        )
    }
}


export default connect(null, {addSong})(SongsForm)