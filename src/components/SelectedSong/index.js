import React from 'react';
import {Card} from 'react-bootstrap'
import {connect} from 'react-redux'

const SelectedSong = (props) => {
    return (

        props.song.name ?

        <Card className="mt-5">
            <Card.Body>

                <Card.Title>
                    {`Selected song: ${props.song.name}`}
                </Card.Title>


                <Card.Text>
                    {`Duration: ${props.song.duration}`}
                </Card.Text>

            </Card.Body>
        </Card>

            :

            <Card className="mt-5">
                <Card.Header>
                    No song selected
                </Card.Header>
            </Card>
    );
};

const mapStateToProps = (state) => {
    return {
        song: state.selectedSong
    }
}


export default connect(mapStateToProps)(SelectedSong);
