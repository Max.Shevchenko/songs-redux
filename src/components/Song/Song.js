import React from 'react';
import {Button} from 'react-bootstrap'
import {connect} from 'react-redux'
import {selectSong} from "../../actions/selectedActions";

const Song = ({name, duration, selectSong}) => {


    const handleClick = () => {
        selectSong({
            name,
            duration
        })
    }


    return (
        <tr key={name}>
            <td className="align-middle text-center">{name}</td>
            <td className="align-middle text-center">{duration}</td>
            <td className="align-middle text-center">
                <Button variant="secondary" onClick={handleClick}>
                    Select song
                </Button>
            </td>
        </tr>
    );
};

export default connect(null, {selectSong})(Song);
