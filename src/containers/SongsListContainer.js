import React, {Component} from 'react'
import {connect} from 'react-redux'
import {getSongs} from "../actions/songsActions";
import SongsList from "../components/SongsList";

class SongsListContainer extends Component {

    render() {
        return (
            <SongsList songs={this.props.songs}/>
        )
    }
}


const mapStateToProps = state => {
    return {
        songs: state.songs
    }
}

export default connect(mapStateToProps, {getSongs})(SongsListContainer)