export const getSongs = () => {
    return {
        type: 'GET_SONGS'
    }
}

export const addSong = song => {
    return {
        type: 'ADD_SONG',
        payload: song
    }
}